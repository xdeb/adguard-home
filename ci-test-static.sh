#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


# TEST: run binary file (x86_64)
./dist/static/amd64/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
./dist/static/amd64/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'

# TEST: run binary file (x86)
./dist/static/386/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
./dist/static/386/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'

# TEST: install .apk package (x86_64)
apk add --allow-untrusted ./dist/adguard-home_${PKG_VERSION}-r${PKG_RELEASE}-xdeb_x86_64.apk
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
/usr/bin/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'
apk del adguard-home

# TEST: install .apk package (x86)
apk add --allow-untrusted ./dist/adguard-home_${PKG_VERSION}-r${PKG_RELEASE}-xdeb_x86.apk
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
/usr/bin/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'
apk del adguard-home

# TEST: upgrade .apk package (x86_64)
apk add --allow-untrusted ./test_data/adguard-home_0.107.37-r1-xdeb_x86_64.apk
/usr/bin/adguard-home --version | grep 'AdGuard Home, version 0.107.37'
apk add --allow-untrusted ./dist/adguard-home_${PKG_VERSION}-r${PKG_RELEASE}-xdeb_x86_64.apk
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
apk del adguard-home

# TEST: upgrade .apk package (x86)
apk add --allow-untrusted ./test_data/adguard-home_0.107.37-r1-xdeb_x86.apk
/usr/bin/adguard-home --version | grep 'AdGuard Home, version 0.107.37'
apk add --allow-untrusted ./dist/adguard-home_${PKG_VERSION}-r${PKG_RELEASE}-xdeb_x86.apk
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
apk del adguard-home
