#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

. /etc/os-release
case $ID in
debian|ubuntu)
    case $VERSION_CODENAME in
    bullseye|bookworm|focal|jammy|noble)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac


################
##            ##
##  DO TESTS  ##
##            ##
################

# TEST: dynamic linked binary
case $CODENAME in
bullseye|focal)
    ./dist/bullseye/amd64/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
    ./dist/bullseye/amd64/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'
    ;;
bookworm|jammy|noble)
    ./dist/bookworm/amd64/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
    ./dist/bookworm/amd64/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac
./dist/bullseye/386/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
./dist/bullseye/386/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'
./dist/bookworm/386/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
./dist/bookworm/386/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'

# TEST: static linked binary
./dist/static/amd64/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
./dist/static/386/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
./dist/static/amd64/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'
./dist/static/386/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'

# TEST: install .deb package (dynamic)
apt-get install -y ./dist/adguard-home_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
/usr/bin/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'

# TEST: uninstall .deb package (dynamic)
apt-get autoremove --purge -y adguard-home
test ! -x /usr/bin/adguard-home
test ! -x /usr/bin/passgen

# TEST: upgrade .deb package (dynamic)
apt-get install -y ./test_data/adguard-home_0.107.37-1-xdeb~${CODENAME}_amd64.deb
/usr/bin/adguard-home --version | grep 'AdGuard Home, version 0.107.37'
apt-get install -y ./dist/adguard-home_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"

# TEST: install .deb package (static)
apt-get install -y ./dist/adguard-home-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
/usr/bin/passgen a-secret | grep '^\$2[ayb]\$.\{56\}$'

# TEST: uninstall .deb package (static)
apt-get autoremove --purge -y adguard-home-static
test ! -x /usr/bin/adguard-home
test ! -x /usr/bin/passgen

# TEST: upgrade .deb package (static)
apt-get install -y ./test_data/adguard-home-static_0.107.37-1-xdeb_amd64.deb
/usr/bin/adguard-home --version | grep 'AdGuard Home, version 0.107.37'
apt-get install -y ./dist/adguard-home-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/adguard-home --version | grep "AdGuard Home, version $PKG_VERSION"
