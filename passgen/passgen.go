package main

import (
	"flag"
	"fmt"
	"os"

	"golang.org/x/crypto/bcrypt"
)

var (
	password string
	cost     int
	verbose  bool
)

func init() {
	flag.Usage = func() {
		fmt.Fprint(flag.CommandLine.Output(), "USAGE: passgen [-cost n] [-v] password\n\n")
		flag.PrintDefaults()
		fmt.Fprint(flag.CommandLine.Output(), "\n")
	}

	flag.IntVar(&cost, "cost", 10, "the hashing cost used to create the given hashed password")
	flag.BoolVar(&verbose, "v", false, "output details")
	flag.Parse()
}

func main() {
	if flag.NArg() != 1 {
		flag.Usage()
		os.Exit(1)
	}
	password = flag.Arg(0)

	crypted, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	if err != nil {
		panic(err)
	}

	if verbose {
		fmt.Printf("password: %s\nbcrypted: %s\n", password, crypted)
	} else {
		fmt.Printf("%s\n", crypted)
	}
}
