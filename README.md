## adguard-home

A network-wide ads & trackers blocking DNS server.

[AdGuard Home](https://github.com/AdguardTeam/AdGuardHome) is a network-wide software for blocking ads & tracking. After you set it up, it'll cover ALL your home devices, and you don't need any client-side software for that.

It operates as a DNS server that re-routes tracking domains to a "black hole," thus preventing your devices from connecting to those servers. It's based on software we use for our public AdGuard DNS servers -- both share a lot of common code.

This repository provides the latest Debian/Ubuntu package.

## Usage (APT installation)

1. Set up the APT key

    Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/adguard-home/pubkey.gpg

    or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/adguard-home/pubkey.asc

2. Set up APT sources

    Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
    content as following (debian/bookworm for example):

        deb https://xdeb.gitlab.io/adguard-home/debian bookworm contrib

    The available source list files are:

    * Debian 11 (bullseye)

        deb https://xdeb.gitlab.io/adguard-home/debian bullseye contrib

    * Debian 12 (bookworm)

        deb https://xdeb.gitlab.io/adguard-home/debian bookworm contrib

    * Ubuntu 20.04 TLS (Focal Fossa)

        deb https://xdeb.gitlab.io/adguard-home/ubuntu focal universe

    * Ubuntu 22.04 TLS (Jammy Jellyfish)

        deb https://xdeb.gitlab.io/adguard-home/ubuntu jammy universe

    * Ubuntu 24.04 LTS (Noble Numbat)

        deb https://xdeb.gitlab.io/adguard-home/ubuntu noble universe

3. Install `adguard-home`

        apt-get update && apt-get install adguard-home

## Usage (download binary)

All binaries are stored at `https://xdeb.gitlab.io/adguard-home/adguard-home_<OS>_<ARCH>.gz`.

For example, you can download x64 binary for _Debian 12_ at <https://xdeb.gitlab.io/adguard-home/adguard-home_bookworm_amd64.gz>. You can also download i386 binary for _Ubuntu 24.04_ at <https://xdeb.gitlab.io/adguard-home/adguard-home_noble_i386.gz>. Then unzip the downloaded file by `gzip -d` command. Change the file permission by `chmod` command finally.

For instance, download the binary on _Debian 12 (amd64)_, and install it, we can use a one-line command like,

    wget -O- https://xdeb.gitlab.io/adguard-home/adguard-home_bookworm_amd64.gz | gzip -d > /usr/local/bin/adguard-home && chmod 755 /usr/local/bin/adguard-home
